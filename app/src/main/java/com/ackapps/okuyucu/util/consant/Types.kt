package com.ackapps.okuyucu.util.consant

class Types {
    enum class AlertTypes(var value: Int) {
        OK(0),
        ERROR(1),
        WARNING(2),
        INFO(3)
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}