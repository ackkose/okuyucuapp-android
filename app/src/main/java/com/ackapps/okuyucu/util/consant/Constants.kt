package com.ackapps.okuyucu.util.consant

class Constants {
    companion object {
        const val FIRE_PERIOD: Long = 5 * 1000
        const val FOREGROUND_SERVICE: String =
            "com.ackapps.okuyucu.service.NotificationForegroundService"
    }

}