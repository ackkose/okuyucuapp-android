package com.ackapps.okuyucu.util.helper

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.ackapps.okuyucu.R
import java.util.*


class NotificationHelper(val context: Context) {

    companion object {

        private val instance: NotificationHelper? = null

        fun instance(context: Context): NotificationHelper {
            if (instance == null)
                return NotificationHelper(context)

            return instance
        }
    }

    fun showService(
        context: Context,
        channelId: String,
        notifyId: Int = 0,
        title: String,
        body: String
    ): Notification {

        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setContentTitle(title)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentText(body)
            .setOngoing(true)
            .setShowWhen(true)
            .setColorized(true)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .setChannelId(channelId)

        return notify(context, notificationBuilder, notifyId)
    }

    private fun notify(
        context: Context,
        notificationBuilder: NotificationCompat.Builder,
        notifyId: Int = 0
    ): Notification {
        var notificationId = notifyId
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "Okuyucu",
                "Okuyucu Seni Dinliyor",
                NotificationManager.IMPORTANCE_HIGH
            )

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            channel.setSound(defaultSoundUri, null)
            channel.vibrationPattern = arrayOf(500L, 500L).toLongArray()
            channel.enableVibration(false)
            channel.importance = NotificationManager.IMPORTANCE_HIGH
            notificationManager.createNotificationChannel(channel)
        }

        if (notificationId == 0)
            notificationId = Random().nextInt(9999)

        val build = notificationBuilder.build()
        notificationManager.notify(notificationId, build)
        return build
    }
}