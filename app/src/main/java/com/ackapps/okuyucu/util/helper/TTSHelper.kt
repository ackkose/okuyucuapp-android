package com.ackapps.okuyucu.util.helper

import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.speech.tts.Voice.LATENCY_VERY_LOW
import android.speech.tts.Voice.QUALITY_VERY_HIGH
import android.util.Log
import com.ackapps.okuyucu.OkuyucuApp
import java.util.*


class TTSHelper : TextToSpeech.OnInitListener {

    private var tts: TextToSpeech? = null

    init {
        try {
            tts = TextToSpeech(OkuyucuApp.instance?.applicationContext, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun speak(text: String) {
        try {
            tts?.speak(text, TextToSpeech.QUEUE_ADD, null, "")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun destroy() {
        try {
            if (tts != null) {
                tts?.stop()
                tts?.shutdown()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onInit(status: Int) {
        try {
            if (status == TextToSpeech.SUCCESS) {
                //val result = tts?.setLanguage(Locale.getDefault())

                val options: MutableSet<String> = HashSet()
                options.add("male")

                val v = Voice(
                    "tr-tr-x-ama-network",
                    Locale("tr", "TR"),
                    QUALITY_VERY_HIGH,
                    LATENCY_VERY_LOW,
                    true,
                    options
                )
                val result = tts?.setVoice(v)

                tts?.setSpeechRate(1.15f)


                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    Log.e(TAG, "Belirlilen dil desteklenmiyor...")
                }
            } else {
                Log.e(TAG, "Cihazınızda TTS desteklenmiyor...")
            }
        } catch (e: Exception) {
            Log.e(TAG, "TTS FAULT")
            e.printStackTrace()
        }
    }

    companion object {

        private const val TAG = "ttsHelperTAG"
        private val instance: TTSHelper? = null

        fun instance(): TTSHelper {
            if (instance == null)
                return TTSHelper()

            return instance
        }
    }


}