package com.ackapps.okuyucu.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment


abstract class BaseFragment<T : ViewDataBinding?, V : BaseViewModel<*>?> : Fragment() {

    val baseActivity: BaseActivity<*, *>? = null

    private var mRootView: View? = null

    private var viewDataBinding: T? = null

    private var mViewModel: V? = null

    abstract val bindingVariable: Int

    private var viewFragment: View? = null

    @get:LayoutRes
    abstract val layoutId: Int
    abstract val viewModel: V
    abstract fun onCreated()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mViewModel = viewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return try {
            if (viewFragment == null) {
                viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
                mRootView = viewDataBinding?.root
                viewFragment = mRootView
            }
            viewFragment
        } catch (error: OutOfMemoryError) {
            viewFragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.setVariable(bindingVariable, mViewModel)
        viewDataBinding?.lifecycleOwner = this
        viewDataBinding?.executePendingBindings()
        onCreated()
    }

    fun binding(): T {
        return viewDataBinding!!
    }


}
