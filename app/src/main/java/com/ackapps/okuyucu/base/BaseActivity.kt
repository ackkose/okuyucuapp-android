package com.ackapps.okuyucu.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.ackapps.okuyucu.R
import com.ackapps.okuyucu.util.consant.Types
import com.tapadoo.alerter.Alerter

abstract class BaseActivity<T : ViewDataBinding?, V : BaseViewModel<*>?> : AppCompatActivity() {

    private var viewDataBinding: T? = null
    private var mViewModel: V? = null

    abstract val bindingVariable: Int

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val viewModel: V

    abstract fun onCreated()

    private var isAlert = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        onCreated()
    }

    fun binding(): T {
        return viewDataBinding!!
    }

    fun showToast(message: String?) {
        Toast.makeText(this, message.toString(), Toast.LENGTH_SHORT).show()
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId)
        mViewModel = if (mViewModel == null) viewModel else mViewModel
        viewDataBinding?.setVariable(bindingVariable, mViewModel)
        viewDataBinding?.executePendingBindings()
    }

    open fun showAlert(
        title: String? = null,
        message: String?,
        alertTypes: Types.AlertTypes = Types.AlertTypes.ERROR
    ) {
        try {
            if (!isAlert) {
                isAlert = true
                var titleText: String? = title
                var color: Int = R.color.red_primary

                when (alertTypes) {
                    Types.AlertTypes.OK -> {
                        //titleText = getString(R.string.alert_title_ok)
                        color = R.color.green_primary
                    }
                    Types.AlertTypes.ERROR -> {
                        //titleText = getString(R.string.alert_title_error)
                        color = R.color.red_primary
                    }
                    Types.AlertTypes.WARNING -> {
                        titleText = getString(R.string.alert_title_warning)
                        color = R.color.orange_primary
                    }
                    Types.AlertTypes.INFO -> {
                        titleText = getString(R.string.alert_title_info)
                        color = R.color.blue_grey_primary
                    }
                }

                if (title != null) {
                    titleText = title
                }

                val alerter = Alerter.create(this)
                    .setText(message.toString())
                    .setIcon(R.drawable.ic_baseline_info_24)
                    .setBackgroundColorRes(color)
                    .setOnHideListener { isAlert = false }

                if (titleText != null)
                    alerter.setTitle(titleText)

                alerter.show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}

