package com.ackapps.okuyucu.base

import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference

abstract class BaseViewModel<N> : ViewModel() {
    private var mNavigator: WeakReference<N>? = null

    var navigator: N?
        get() = if (mNavigator != null) mNavigator!!.get() else null
        set(navigator) {
            mNavigator = WeakReference(navigator)
        }

}
