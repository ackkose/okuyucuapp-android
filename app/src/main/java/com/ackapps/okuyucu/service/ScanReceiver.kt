package com.ackapps.okuyucu.service

import android.app.ActivityManager
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.ackapps.okuyucu.OkuyucuApp
import com.ackapps.okuyucu.util.consant.Constants

class ScanReceiver : BroadcastReceiver() {

    private var mContext: Context? = null
    private val PENDING_INTENT_REQUEST_CODE = 333

    override fun onReceive(context: Context, intent: Intent) {
        mContext = context
        try {
            val alarmManager =
                mContext!!.applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(mContext!!.applicationContext, ScanReceiver::class.java)
            alarmIntent.putExtra("control", false)

            val pendingIntent = PendingIntent.getBroadcast(
                mContext!!.applicationContext,
                PENDING_INTENT_REQUEST_CODE,
                alarmIntent,
                0
            )
            alarmManager[AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + Constants.FIRE_PERIOD] =
                pendingIntent

            if (!isServiceRunning(Constants.FOREGROUND_SERVICE)) {
                NotificationForegroundService.startService(context)
                OkuyucuApp.instance?.isServiceStart?.value = true
            } else {
                //Log.e("ForegroundService:::", "Working")
                OkuyucuApp.instance?.isServiceStart?.value = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun isServiceRunning(serviceName: String): Boolean {
        var isServiceRunning = false
        val am = mContext!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val l = am.getRunningServices(Int.MAX_VALUE)
        val i: Iterator<ActivityManager.RunningServiceInfo> = l.iterator()
        while (i.hasNext()) {
            val runningServiceInfo = i.next()
            if (runningServiceInfo.service.className == serviceName) {
                if (runningServiceInfo.foreground) {
                    isServiceRunning = true
                }
            }
        }
        return isServiceRunning
    }
}
