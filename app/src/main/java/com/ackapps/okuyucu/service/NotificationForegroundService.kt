package com.ackapps.okuyucu.service

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import com.ackapps.okuyucu.OkuyucuApp
import com.ackapps.okuyucu.util.helper.NotificationHelper
import com.ackapps.okuyucu.util.helper.TTSHelper
import com.google.gson.Gson


class NotificationForegroundService : NotificationListenerService() {

    private var isSendNotification = false
    private var notificationHelper: NotificationHelper? = null
    private var notificationManager: NotificationManager? = null
    private var ttsHelper: TTSHelper? = null
    private val channelId = "Okuyucu"
    private val notifyId = 6589
    private val createdAt: Long = System.currentTimeMillis()
    private val readNotificationArray = mutableListOf<Int>()

    override fun onCreate() {
        super.onCreate()
        ttsHelper = TTSHelper.instance()
        notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationHelper = NotificationHelper(applicationContext)

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!isSendNotification) {
            startNotification()
            isSendNotification = !isSendNotification
        }

        return START_REDELIVER_INTENT
    }


    override fun onNotificationPosted(sbn: StatusBarNotification) {
        Log.e("Notification", Gson().toJson(sbn))
        sbn.let {
            if (OkuyucuApp.instance?.isSilenceMode?.value == false) {
                if ((it.packageName != packageName || (it.packageName == packageName && it.id != 6589)) && it.postTime > createdAt) {
                    if (!readNotificationArray.contains(it.id) && !it.packageName.contains("android")) {
                        Log.e("Notification", "read")
                        if (readNotificationArray.size > 1000) readNotificationArray.clear()
                        readNotificationArray.add(it.id)
                        speakNotification(
                            it.notification.extras.getString("android.title").toString(),
                            it.notification.extras.getString("android.text").toString()
                        )
                    } else Log.e("Notification", "not read")
                } else Log.e("Notification", "not read")
            }else Log.e("Notification", "muted")
        }
        super.onNotificationPosted(sbn)
    }

    private fun speakNotification(title: String, content: String) {
        ttsHelper?.speak("Yeni bir bildiriminiz var").also {
            Handler(mainLooper).postDelayed({
                ttsHelper?.speak("Başlık: $title").also {
                    Handler(mainLooper).postDelayed({
                        ttsHelper?.speak("Mesaj: $content")
                    }, 1000)
                }
            }, 650)
        }
    }

    private fun startNotification() {
        try {
            val notification = notificationHelper?.showService(
                OkuyucuApp.instance?.applicationContext!!,
                channelId,
                notifyId,
                "Okuyucu",
                "Bildirim Servisleri Açık"
            )
            startForeground(notifyId, notification)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (notificationManager != null) {
            notificationManager?.cancel(notifyId)
        }
    }

    companion object {

        fun startService(context: Context) {
            try {
                val serviceIntent = Intent(context, NotificationForegroundService::class.java)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(serviceIntent)
                } else {
                    context.startService(serviceIntent)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}