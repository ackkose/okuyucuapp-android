package com.ackapps.okuyucu.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.ackapps.okuyucu.util.consant.Constants

class BootReceiver : BroadcastReceiver() {
    private val PENDING_INTENT_REQUEST_CODE = 333
    override fun onReceive(context: Context, intent: Intent) {
        //Log.i("alarmStatus", "received")
        if (Intent.ACTION_BOOT_COMPLETED == intent.action || "android.intent.action.BOOT_COMPLETED" == intent.action || "android.intent.action.QUICKBOOT_POWERON" == intent.action) {
            startServiceByAlarm(context)
        }
    }

    private fun startServiceByAlarm(context: Context) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmIntent = Intent(context.applicationContext, ScanReceiver::class.java)

        if (PendingIntent.getBroadcast(
                context.applicationContext,
                PENDING_INTENT_REQUEST_CODE,
                alarmIntent,
                PendingIntent.FLAG_NO_CREATE
            ) != null
        ) {
            //Log.i("alarmStatus", "active")
        } else {
            val pendingIntent = PendingIntent.getBroadcast(
                context.applicationContext,
                PENDING_INTENT_REQUEST_CODE,
                alarmIntent,
                0
            )
            alarmManager.setAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + Constants.FIRE_PERIOD,
                pendingIntent
            )
            //Log.i("alarmStatus", "passive")
        }
    }
}
