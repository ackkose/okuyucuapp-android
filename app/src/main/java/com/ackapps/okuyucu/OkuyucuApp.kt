package com.ackapps.okuyucu

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class OkuyucuApp : Application() {

    val isServiceStart: MutableLiveData<Boolean> = MutableLiveData(false)
    val isServiceStartStatus: LiveData<Boolean> = isServiceStart
    val isSilenceMode: MutableLiveData<Boolean> = MutableLiveData(false)

    override fun onCreate() {
        super.onCreate()
        instance = this

    }

    companion object {
        @get:Synchronized
        var instance: OkuyucuApp? = null
            private set
    }

}