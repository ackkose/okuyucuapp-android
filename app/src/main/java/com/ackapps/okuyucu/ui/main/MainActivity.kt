package com.ackapps.okuyucu.ui.main

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.PowerManager
import android.provider.Settings
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.ackapps.okuyucu.BR
import com.ackapps.okuyucu.OkuyucuApp
import com.ackapps.okuyucu.R
import com.ackapps.okuyucu.base.BaseActivity
import com.ackapps.okuyucu.databinding.ActivityMainBinding
import com.ackapps.okuyucu.service.NotificationForegroundService
import com.ackapps.okuyucu.service.ScanReceiver
import kotlin.random.Random


class MainActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>(), MainNavigator {

    private var socketIntent: Intent? = null
    private lateinit var serviceAlarmIntent: Intent
    private var pendingIntent: PendingIntent? = null
    private var alarmManager: AlarmManager? = null
    private var isServiceRestarted = false
    private var notificationChannel = "okuyucuApp"
    private val allowedChars = "0123456789qwertyuiopasdfghjklzxcvbnm"


    override val bindingVariable: Int
        get() = BR.mainVM

    override val layoutId: Int
        get() = R.layout.activity_main

    override val viewModel: MainActivityViewModel
        get() = ViewModelProvider(this)[MainActivityViewModel::class.java]

    override fun onCreated() {
        viewModel.navigator = this
        socketIntent = Intent(this, NotificationForegroundService::class.java)
        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if (OkuyucuApp.instance?.isSilenceMode?.value == true) {
            binding().imageView.setImageResource(R.drawable.ic_baseline_volume_off_24)
        } else {
            binding().imageView.setImageResource(R.drawable.ic_baseline_volume_up_24)
        }


        subscribeToObservables()
        powerManager()
        setupNotification()


        if (!Settings.Secure.getString(this.contentResolver, "enabled_notification_listeners")
                .contains(applicationContext.packageName)
        ) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                startActivity(Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS))
            } else {
                startActivity(Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))
            }
        }

        binding().imageView.setOnClickListener {
            OkuyucuApp.instance?.isSilenceMode?.value =
                !(OkuyucuApp.instance?.isSilenceMode?.value == true)
        }
    }

    private var serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            try {
                //val myService = (service as NotificationForegroundService.LocalBinder).service
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {}
    }

    @SuppressLint("SetTextI18n")
    private fun subscribeToObservables() {
        OkuyucuApp.instance?.isServiceStartStatus?.observe(this) { status ->
            status?.let {
                if (it) {
                    binding().textView.text = "Bildirim Servisi : Aktif"
                    binding().textView.setOnClickListener {
                        Toast.makeText(
                            applicationContext,
                            "Bildirimler Dinleniyor.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    binding().textView.text = "Bildirim Servisi : Pasif"
                    binding().textView.setOnClickListener {
                        if (!isServiceRestarted) {
                            isServiceRestarted = true
                            restartService()
                            Handler(mainLooper).postDelayed({ isServiceRestarted = false }, 5000L)
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Servis Yeniden Başlatılıyor...",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }
        OkuyucuApp.instance?.isSilenceMode?.observe(this) {
            if (it) {
                binding().imageView.setImageResource(R.drawable.ic_baseline_volume_off_24)
            } else {
                binding().imageView.setImageResource(R.drawable.ic_baseline_volume_up_24)
            }
        }
    }

    private fun onServiceStart(isForceAlarm: Boolean = false) {
        try {
            serviceAlarmIntent = Intent(applicationContext, ScanReceiver::class.java)
            serviceAlarmIntent.putExtra("control", false)

            val isBroadcast = PendingIntent.getBroadcast(
                applicationContext,
                333,
                serviceAlarmIntent,
                PendingIntent.FLAG_NO_CREATE
            )
            if (isBroadcast == null) {
                pendingIntent =
                    PendingIntent.getBroadcast(applicationContext, 333, serviceAlarmIntent, 0)
                assert(alarmManager != null)
                alarmManager?.setAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + 3 * 1000,
                    pendingIntent
                )
            }

            if (isForceAlarm) {
                pendingIntent =
                    PendingIntent.getBroadcast(applicationContext, 333, serviceAlarmIntent, 0)
                assert(alarmManager != null)
                alarmManager?.setAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + 3 * 1000,
                    pendingIntent
                )
            }

            socketIntent = Intent(this, NotificationForegroundService::class.java)

            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
            startService(intent)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun onServiceStop() {
        stopService(socketIntent)
        try {
            unbindService(serviceConnection)
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun restartService() {
        try {
            onServiceStop()
            if (isServiceStatus()) {
                onServiceStart(isForceAlarm = true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()

        onServiceStart(isServiceStatus())
    }

    private fun isServiceStatus(): Boolean {
        try {
            val intent = Intent(this, ScanReceiver::class.java)
            intent.putExtra("control", false)
            return PendingIntent.getBroadcast(
                applicationContext,
                333,
                intent,
                PendingIntent.FLAG_NO_CREATE
            ) != null
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return false
    }

    @SuppressLint("InvalidWakeLockTag", "WakelockTimeout", "BatteryLife")
    fun powerManager() {
        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager?
        if (pm != null && !pm.isIgnoringBatteryOptimizations(packageName)) {
            val intent = Intent()
            intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            intent.data = Uri.parse("package:${packageName}")
            startActivityForResult(intent, 588)
        }

        try {
            val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager?
            val wl = powerManager?.newWakeLock(
                PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP,
                "okuyucuApp"
            )
            wl?.acquire()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun setupNotification() {
        binding().sendNotificationBTN.isVisible = true
        binding().sendNotificationBTN.setOnClickListener {
            val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val mBuilder: NotificationCompat.Builder =
                NotificationCompat.Builder(this@MainActivity, notificationChannel)
            mBuilder.setContentTitle("Bu Bir Deneme Bildirimidir.")
            mBuilder.setContentText("Deneme")
            mBuilder.setTicker("ACK")
            mBuilder.setSmallIcon(R.drawable.ic_launcher_foreground)
            mBuilder.setAutoCancel(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_HIGH
                val notificationChannel = NotificationChannel(
                    notificationChannel,
                    "FakeNotification",
                    importance
                )
                mBuilder.setChannelId(this.notificationChannel)
                mNotificationManager.createNotificationChannel(notificationChannel)
            }
            mNotificationManager.notify(System.currentTimeMillis().toInt(), mBuilder.build())
        }
    }

    private fun getRandomString(sizeOfRandomString: Int): String {
        val sb = StringBuilder(sizeOfRandomString)
        for (i in 0 until sizeOfRandomString) sb.append(
            allowedChars[Random.nextInt(
                allowedChars.length
            )]
        )
        return sb.toString()
    }
}